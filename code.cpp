﻿#define _USE_MATH_DEFINES
#define _CRT_SECURE_NO_WARNINGS

#include <cstdio>
#include <cmath>
#include <fstream>
#include <iostream>
#include <iomanip>
#include <vector>
#include <iterator>

using namespace std;

template<class T> void PrintVectorAsMatrix(const vector<T>& data, size_t cols, ostream& out = cout);
void initial_condition(vector<double>& V, vector<double>& W, vector<double>& P, int n);
void get_f1(vector<double>& f1, vector<double>& V, int n, double tau, int k);
void get_f2(vector<double>& f2, vector<double>& W, int n, double tau, int k);
void get_A(vector<double>& A, int n, int m, int T);
void inverse_A(vector<double>& A, vector<double>& Ai, int n, int m, int T);
void get_B(vector<double>& B1, vector<double>& B2, int n, int m, int T);
void get_equation(vector<double>& S, vector<double>& b, vector<double>& Ai, vector<double>& B1, vector<double>& B2, vector<double>& f1, vector<double>& f2, int n, int m, int T);
void find_tau(vector<double>& tau, int matrix_n, int tau_n, double max);
void optimal_transposition(vector<int>& T, int p);
void iteration_with_transposition(vector<double>& P, vector<double>& S, vector<double>& b, double d, int shift, int matrix_n);
void calculate_V(vector<double>& V, vector<double>& Ai, vector<double>& B1, vector<double>& P, vector<double>& f1, int k, int n);
void calculate_W(vector<double>& W, vector<double>& Ai, vector<double>& B2, vector<double>& P, vector<double>& f2, int k, int n);
void solve(vector<double>& V, vector<double>& W, vector<double>& P, int n, int m, int T);
void residual(vector<double>& V, vector<double>& W, vector<double>& P, int n, int m, int T);
void output(vector<double>& V, vector<double>& W, vector<double>& P, int n, int m, int T);

double check(vector<double>& P, vector<double>& S, vector<double>& b, int shift, int matrix_n);

int main()
{
    int T = 1;
    int n, m;
    cout << "N = ";
    cin >> n;
    if (n < 3)
    {
        cout << "Wrong N" << endl;
        return -1;
    }
    cout << "M = ";
    cin >> m;
    if (m < 1)
    {
        cout << "Wrong M" << endl;
        return -1;
    }
    vector<double> V((n - 1) * (n - 1) * (m + 1)), W((n - 1) * (n - 1) * (m + 1)), P(n * n * (m + 1));
    solve(V, W, P, n, m, T);
    residual(V, W, P, n, m, T);
    output(V, W, P, n, m, T);
}

template<class T> void PrintVectorAsMatrix(const vector<T>& data, size_t cols, ostream& out)
{
    for (auto it = data.begin(); it < data.end(); it += cols) {
        copy(it, it + cols, ostream_iterator<T>(out, " "));
        out << endl;
    }
}

void initial_condition(vector<double>& V, vector<double>& W, vector<double>& P, int n)
{
    double h = 1. / n;
    for (int j = 0; j < n - 1; ++j)
    {
        for (int i = 0; i < n - 1; ++i)
        {
            V[j * (n - 1) + i] = 0;
            W[j * (n - 1) + i] = 0;
        }
    }
    for (int j = 0; j < n; ++j)
    {
        for (int i = 0; i < n; ++i)
        {
            P[j * n + i] = 0;
        }
    }
}

void get_f1(vector<double>& f1, vector<double>& V, int n, double tau, int k)
{
    double h = 1. / n;
    for (int j = 0; j < n - 1; ++j)
    {
        for (int i = 0; i < n - 1; ++i)
        {
            if (j == 0) {
                f1[j * (n - 1) + i] = n * sqrt(n);
                f1[j * (n - 1) + i] *= tau;
                f1[j * (n - 1) + i] += V[k * (n - 1) * (n - 1) + j * (n - 1) + i];
            }
            else {
                f1[j * (n - 1) + i] = 0;
                f1[j * (n - 1) + i] *= tau;
                f1[j * (n - 1) + i] += V[k * (n - 1) * (n - 1) + j * (n - 1) + i];
            }

        }
    }
}

void get_f2(vector<double>& f2, vector<double>& W, int n, double tau, int k)
{
    double h = 1. / n;
    for (int j = 0; j < n - 1; ++j)
    {
        for (int i = 0; i < n - 1; ++i)
        {
            if (i == 1) {
                f2[j * (n - 1) + i] = 0;
                f2[j * (n - 1) + i] *= tau;
                f2[j * (n - 1) + i] += W[k * (n - 1) * (n - 1) + j * (n - 1) + i];
            }
            else {
                f2[j * (n - 1) + i] = 0;
                f2[j * (n - 1) + i] *= tau;
                f2[j * (n - 1) + i] += W[k * (n - 1) * (n - 1) + j * (n - 1) + i];
            }

        }
    }
}

void get_A(vector<double>& A, int n, int m, int T)
{
    double tau = (1. / m) * T;
    double h = 1. / n;
    double c = tau * n * n;
    int nn = (n - 1) * (n - 1);
    for (int i = 0; i < nn; ++i)
    {
        for (int j = 0; j < nn; ++j)
        {
            if (i == j) {
                A[j * nn + i] = 1 + 4 * c;
            }
            else if (fabs(i - j) == 1) {
                if ((((i + j + 1) / 2) % (n - 1)) == 0) {
                    A[j * nn + i] = 0;
                }
                else {
                    A[j * nn + i] = -c;
                }
            }
            else if (fabs(i - j) == (n - 1)) {
                A[j * nn + i] = -c;
            }
            else {
                A[j * nn + i] = 0;
            }
        }
    }
}

void inverse_A(vector<double>& A, vector<double>& Ai, int n, int m, int T)
{
    double tau = (1. / m) * T;
    double h = 1. / n;
    double c = tau * n * n;
    double d = 1 / c;
    double div, multi;
    int nn = (n - 1) * (n - 1);
    for (int i = 0; i < nn; ++i)
    {
        for (int j = 0; j < nn; ++j)
        {
            Ai[j * nn + i] = 0;
        }
        Ai[i * nn + i] = 1;
    }
    for (int k = 0; k < nn; ++k)
    {
        for (int i = k + 1; i < nn; ++i)
        {
            if (fabs(A[k * nn + i]) > 1e-12)
            {
                for (int j = 0; j < nn; ++j)
                {
                    swap(A[j * nn + i], A[j * nn + k]);
                    swap(Ai[j * nn + i], Ai[j * nn + k]);
                }
                break;
            }
        }
        div = A[k * nn + k];
        for (int j = 0; j < nn; ++j)
        {
            A[j * nn + k] /= div;
            Ai[j * nn + k] /= div;
        }
        for (int i = k + 1; i < nn; ++i)
        {
            multi = A[k * nn + i];
            for (int j = 0; j < nn; ++j)
            {
                A[j * nn + i] -= multi * A[j * nn + k];
                Ai[j * nn + i] -= multi * Ai[j * nn + k];
            }
        }
    }
    for (int k = nn - 1; k > 0; --k)
    {
        for (int i = k - 1; i + 1 > 0; --i)
        {
            multi = A[k * nn + i];
            for (int j = 0; j < nn; ++j)
            {
                A[j * nn + i] -= multi * A[j * nn + k];
                Ai[j * nn + i] -= multi * Ai[j * nn + k];
            }
        }
    }
}

void get_B(vector<double>& B1, vector<double>& B2, int n, int m, int T)
{
    double tau = (1. / m) * T;
    double h = 1. / n;
    double c = tau * n;
    for (int i1 = 0; i1 < n; ++i1)
    {
        for (int j1 = 0; j1 < n - 1; ++j1)
        {
            if (i1 == (j1 + 1)) {
                for (int i2 = 0; i2 < n; ++i2)
                {
                    for (int j2 = 0; j2 < n - 1; ++j2)
                    {
                        if (i2 == j2) {
                            B1[j1 * (n - 1) * n * n + j2 * n * n + i1 * n + i2] = -c;
                        }
                        else if (i2 == (j2 + 1)) {
                            B1[j1 * (n - 1) * n * n + j2 * n * n + i1 * n + i2] = c;
                        }
                        else {
                            B1[j1 * (n - 1) * n * n + j2 * n * n + i1 * n + i2] = 0;
                        }
                    }
                }
            }
            else {
                for (int i2 = 0; i2 < n; ++i2)
                {
                    for (int j2 = 0; j2 < n - 1; ++j2)
                    {
                        B1[j1 * (n - 1) * n * n + j2 * n * n + i1 * n + i2] = 0;
                    }
                }
            }
        }
    }
    for (int i1 = 0; i1 < n; ++i1)
    {
        for (int j1 = 0; j1 < n - 1; ++j1)
        {
            if (i1 == j1) {
                for (int i2 = 0; i2 < n; ++i2)
                {
                    for (int j2 = 0; j2 < n - 1; ++j2)
                    {
                        if (i2 == j2 + 1) {
                            B2[j1 * (n - 1) * n * n + j2 * n * n + i1 * n + i2] = -c;
                        }
                        else {
                            B2[j1 * (n - 1) * n * n + j2 * n * n + i1 * n + i2] = 0;
                        }
                    }
                }
            }
            else if (i1 == j1 + 1) {
                for (int i2 = 0; i2 < n; ++i2)
                {
                    for (int j2 = 0; j2 < n - 1; ++j2)
                    {
                        if (i2 == j2 + 1) {
                            B2[j1 * (n - 1) * n * n + j2 * n * n + i1 * n + i2] = c;
                        }
                        else {
                            B2[j1 * (n - 1) * n * n + j2 * n * n + i1 * n + i2] = 0;
                        }
                    }
                }
            }
            else {
                for (int i2 = 0; i2 < n; ++i2)
                {
                    for (int j2 = 0; j2 < n - 1; ++j2)
                    {
                        B2[j1 * (n - 1) * n * n + j2 * n * n + i1 * n + i2] = 0;
                    }
                }
            }
        }
    }
}

void get_equation(vector<double>& S, vector<double>& b, vector<double>& Ai, vector<double>& B1, vector<double>& B2, vector<double>& f1, vector<double>& f2, int n, int m, int T)
{
    int nn = (n - 1) * (n - 1);
    vector<double> T1(n * n * nn), T2(n * n * nn);
    for (int j = 0; j < n * n; ++j)
    {
        for (int i = 0; i < nn; ++i)
        {
            T1[j * nn + i] = 0;
            T2[j * nn + i] = 0;
            for (int k = 0; k < nn; ++k)
            {
                T1[j * nn + i] += B1[k * n * n + j] * Ai[k * nn + i];
                T2[j * nn + i] += B2[k * n * n + j] * Ai[k * nn + i];
            }
        }
    }
    for (int j = 1; j < n * n; ++j)
    {
        for (int i = 1; i < n * n; ++i)
        {
            S[(j - 1) * (n * n - 1) + i - 1] = 0;
            for (int k = 0; k < nn; ++k)
            {
                S[(j - 1) * (n * n - 1) + i - 1] += T1[j * nn + k] * B1[k * n * n + i];
                S[(j - 1) * (n * n - 1) + i - 1] += T2[j * nn + k] * B2[k * n * n + i];
            }
        }
        b[j - 1] = 0;
        for (int s = 0; s < nn; ++s)
        {
            b[j - 1] += T1[j * nn + s] * f1[s];
            b[j - 1] += T2[j * nn + s] * f2[s];
        }
    }
}

void find_tau(vector<double>& tau, int matrix_n, int tau_n, double max)
{
    double root;
    double m = 0.01, M = max;
    for (int i = 0; i < tau_n; ++i)
    {
        root = cos(M_PI * (2 * i + 1) * 1. / (2 * tau_n));
        tau[i] = 1 / (0.5 * (m + M) + 0.5 * (M - m) * root);
    }
}

void optimal_transposition(vector<int>& T, int p)
{
    int k = pow(2, p);
    int k1 = pow(2, p - 1);
    vector<int> T0(k1);
    if (p > 1)
    {
        optimal_transposition(T0, p - 1);
        for (int i = 0; i < k1; ++i)
        {
            T[2 * i] = k + 1 - T0[i];
            T[2 * i + 1] = T0[i];
        }
    }
    else {
        T[0] = 2;
        T[1] = 1;
    }
}

void iteration_with_transposition(vector<double>& P, vector<double>& S, vector<double>& b, double d, int shift, int matrix_n)
{
    int iter_n = 10 * matrix_n;
    int p = 4;
    int tau_n = pow(2, p);
    double s;
    vector<double> SP(matrix_n);
    vector<double> tau(tau_n);
    vector<int> T(tau_n);

    double tmp, max = 0;
    for (int j = 0; j < matrix_n; ++j)
    {
        tmp = 0;
        for (int i = 0; i < matrix_n; ++i)
        {
            if (i != j)
            {
                tmp += fabs(S[j * matrix_n + i]);
            }
        }
        if (S[j * matrix_n + j] + tmp > max) max = S[j * matrix_n + j] + tmp;
    }

    find_tau(tau, matrix_n, tau_n, max);
    optimal_transposition(T, p);
    for (int j = 0; j < matrix_n; ++j)
    {
        P[shift + j] = 1;
    }
    for (int k = 0; k < iter_n; ++k)
    {
        for (int j = 0; j < matrix_n; ++j)
        {
            s = 0;
            for (int i = 0; i < matrix_n; ++i)
            {
                s += S[j * matrix_n + i] * P[shift + i];
            }
            SP[j] = s;
        }

        for (int j = 0; j < matrix_n; ++j)
        {
            P[shift + j] += tau[(T[k % tau_n] - 1)] * (b[j] - SP[j]);
        }
    }
}

void calculate_V(vector<double>& V, vector<double>& Ai, vector<double>& B1, vector<double>& P, vector<double>& f1, int k, int n)
{
    int nn = (n - 1) * (n - 1);
    for (int j = 0; j < nn; ++j)
    {
        for (int s = 0; s < n * n; ++s)
        {
            f1[j] -= B1[j * n * n + s] * P[(k + 1) * n * n + s];
        }
    }
    for (int j = 0; j < nn; ++j)
    {
        V[(k + 1) * nn + j] = 0;
        for (int i = 0; i < nn; ++i)
        {
            V[(k + 1) * nn + j] += Ai[j * nn + i] * f1[i];
        }
    }
}

void calculate_W(vector<double>& W, vector<double>& Ai, vector<double>& B2, vector<double>& P, vector<double>& f2, int k, int n)
{
    int nn = (n - 1) * (n - 1);
    for (int j = 0; j < nn; ++j)
    {
        for (int s = 0; s < n * n; ++s)
        {
            f2[j] -= B2[j * n * n + s] * P[(k + 1) * n * n + s];
        }
    }
    for (int j = 0; j < nn; ++j)
    {
        W[(k + 1) * nn + j] = 0;
        for (int i = 0; i < nn; ++i)
        {
            W[(k + 1) * nn + j] += Ai[j * nn + i] * f2[i];
        }
    }
}

void solve(vector<double>& V, vector<double>& W, vector<double>& P, int n, int m, int T)
{
    double tau = (1. / m) * T;
    double h = 1. / n;
    int nn = (n - 1) * (n - 1);
    vector<double> A(nn * nn), Ai(nn * nn), B1(nn * n * n), B2(nn * n * n), S((n * n - 1) * (n * n - 1));
    vector<double> f1(nn), f2(nn), b(n * n - 1);
    initial_condition(V, W, P, n);
    get_A(A, n, m, T);
    //PrintVectorAsMatrix(A, nn, cout);
    inverse_A(A, Ai, n, m, T);
    get_B(B1, B2, n, m, T);
    //PrintVectorAsMatrix(B2, n * n, cout);
    for (int k = 0; k < m; ++k)
    {
        get_f1(f1, V, n, tau, k);
        get_f2(f2, W, n, tau, k);
        get_equation(S, b, Ai, B1, B2, f1, f2, n, m, T);
        //PrintVectorAsMatrix(S, n * n - 1, cout);
        iteration_with_transposition(P, S, b, tau / h, (k + 1) * n * n + 1, n * n - 1);
        cout << "Iteration residual = " << scientific << check(P, S, b, (k + 1) * n * n + 1, n * n - 1) << endl;
        calculate_V(V, Ai, B1, P, f1, k, n);
        calculate_W(W, Ai, B2, P, f2, k, n);
    }
}

void residual(vector<double>& V, vector<double>& W, vector<double>& P, int n, int m, int T)
{
    double tau = (1. / m) * T;
    double h = 1. / n;
    double tmp1, tmp2, tmp3, residual1 = 0, residual2 = 0, residual3 = 0;
    ofstream fout("residual.txt");
    int nn = (n - 1) * (n - 1);
    vector<double> A(nn * nn), B1(nn * n * n), B2(nn * n * n);
    vector<double> f1(nn), f2(nn);
    vector<double> R(m * 3);
    get_A(A, n, m, T);
    get_B(B1, B2, n, m, T);
    for (int k = 0; k < m; ++k)
    {
        get_f1(f1, V, n, tau, k);
        get_f2(f2, W, n, tau, k);
        for (int j = 0; j < nn; ++j)
        {
            tmp1 = 0;
            tmp2 = 0;
            for (int i = 0; i < nn; ++i)
            {
                tmp1 += A[j * nn + i] * V[(k + 1) * nn + i];
                tmp2 += A[j * nn + i] * W[(k + 1) * nn + i];
            }
            for (int s = 0; s < n * n; ++s)
            {
                tmp1 += B1[j * n * n + s] * P[(k + 1) * n * n + s];
                tmp2 += B2[j * n * n + s] * P[(k + 1) * n * n + s];
            }
            R[k * 3] += fabs(tmp1 - f1[j]);
            R[k * 3 + 1] += fabs(tmp2 - f2[j]);
        }
        residual1 += R[k * 3];
        residual2 += R[k * 3 + 1];
        R[k * 3] = -log10(R[k * 3]);
        R[k * 3 + 1] = -log10(R[k * 3 + 1]);
        for (int j = 0; j < n * n; ++j)
        {
            tmp3 = 0;
            for (int i = 0; i < nn; ++i)
            {
                tmp3 += B1[i * n * n + j] * V[(k + 1) * nn + i];
                tmp3 += B2[i * n * n + j] * W[(k + 1) * nn + i];
            }
            R[k * 3 + 2] += fabs(tmp3);
        }
        residual3 += R[k * 3 + 2];
        R[k * 3 + 2] = -log10(R[k * 3 + 2]);
    }
    cout << "Equation 1 residual = " << scientific << residual1 << endl;
    cout << "Equation 2 residual = " << scientific << residual2 << endl;
    cout << "Equation 3 residual = " << scientific << residual3 << endl;
    int k = 1;
    for (auto it = R.begin(); it < R.end(); it += 3, ++k) {
        fout << k << " " << *it << " " << *(it+1) << " " << *(it+2) << endl;
    }
}

void output(vector<double>& V, vector<double>& W, vector<double>& P, int n, int m, int T)
{
    int step = 1;
    double tau = (1. / m) * T;
    double h = 1. / n;
    ofstream fout("output.txt");
    vector<double> V0((n + 1) * (n + 1) * (m + 1)), W0((n + 1) * (n + 1) * (m + 1));
    vector<double> V1(n * n * (m + 1)), W1(n * n * (m + 1)), X(n * n), Y(n * n);
    for (int k = 0; k < m + 1; ++k)
    {
        for (int j = 0; j < n + 1; ++j)
        {
            for (int i = 0; i < n + 1; ++i)
            {
                if ((i == 0) || (i == n) || (j == 0) || (j == n)) {
                    V0[k * (n + 1) * (n + 1) + j * (n + 1) + i] = 0;
                    W0[k * (n + 1) * (n + 1) + j * (n + 1) + i] = 0;
                }
                else {
                    V0[k * (n + 1) * (n + 1) + j * (n + 1) + i] = V[k * (n - 1) * (n - 1) + (j - 1) * (n - 1) + i - 1];
                    W0[k * (n + 1) * (n + 1) + j * (n + 1) + i] = W[k * (n - 1) * (n - 1) + (j - 1) * (n - 1) + i - 1];
                }
            }
        }
    }
    for (int k = 0; k < m + 1; ++k)
    {
        for (int j = 0; j < n; ++j)
        {
            for (int i = 0; i < n; ++i)
            {
                V1[k * n * n + j * n + i] = (V0[k * (n + 1) * (n + 1) + j * (n + 1) + i] + V0[k * (n + 1) * (n + 1) + j * (n + 1) + i + 1]) / 2;
                W1[k * n * n + j * n + i] = (W0[k * (n + 1) * (n + 1) + j * (n + 1) + i] + W0[k * (n + 1) * (n + 1) + (j + 1) * (n + 1) + i]) / 2;
            }
        }
    }
    for (int j = 0; j < n; ++j)
    {
        for (int i = 0; i < n; ++i)
        {
            X[j * n + i] = i * h;
            Y[j * n + i] = j * h;
        }
    }
    int shift = n * n;
    for (int k = 1; k < m + 1; k += step) 
    {
        for (auto it1 = X.begin(), it2 = Y.begin(), it3 = V1.begin() + shift, it4 = W1.begin() + shift, it5 = P.begin() + shift;
            it1 < X.end(); ++it1, ++it2, ++it3, ++it4, ++it5) {
            fout << *it1 << " " << *it2 << " " << *it3 << " " << *it4 << " " << *it5 << " " << endl;
        }
        fout << endl << endl;
        shift += step * n * n;
    }
    
}

double check(vector<double>& P, vector<double>& S, vector<double>& b, int shift, int matrix_n)
{
    vector<double> SP(matrix_n);
    double residual = 0;
    for (int j = 0; j < matrix_n; ++j)
    {
        SP[j] = 0;
        for (int i = 0; i < matrix_n; ++i)
        {
            SP[j] += S[j * matrix_n + i] * P[shift + i];
        }
    }
    for (int j = 0; j < matrix_n; ++j)
    {
        residual += fabs(SP[j] - b[j]);
    }
    return residual;
}
